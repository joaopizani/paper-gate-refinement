default :
	lhs2TeX --agda main.lagda -o main.tex
	TEXMFHOME=./texmf:${TEXMFHOME} latexmk -pdf main.tex

clean:
	latexmk -c main.tex
	rm main.tex
	rm -f comment.cut

