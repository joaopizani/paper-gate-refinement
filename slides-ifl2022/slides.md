---
author: João Paulo Pizani Flor, Wouter Swierstra
title: Verified Technology Mapping in an Agda DSL for Circuit Design
institute: Utrecht University
theme: metropolis
mainfont: Open Sans
mainfontoptions: Scale=0.8
sansfont: Open Sans
sansfontoptions: Scale=0.8
monofont: PragmataPro Mono
monofontoptions: Scale=0.8
classoption: aspectratio=169
documentclass: beamer
---

## Circuit design & verification

* Designing modern application specific integrated circuits is an
  *extremely* complicated endeavour.
 
. . . 
 
* And making mistakes is very costly indeed.

. . .

* To make matters even worse, most hardware description languages are not
  designed with verification in mind.
  
. . .

* **Question:** Can we do better?

---

## Challenge -- technology mapping & refinement

Like programming, circuit designers rarely find the final design at
once.

Instead, the process is *iterative*, starting with an (executable)
specification of the circuit.

Each iteration may add additional layers of cache, perform certain
optimizations, map a given component to piece of proprietary hardware,
or otherwise transform the initial design.

. . .

**Challenge:** Can we find a language that enables us to reason about this process?

. . . 

**Non goal:** verify realistic modern hardware.

---

## Technology mapping

In particular, we are interested in the process of *technology
mapping*.

Here we may want to reuse certain (third-party) components to
*implement* certain functionality.

Under which conditions can we substitute one circuit for another?

. . . 
 
But as functional programmers, we do this all the time when we do
*equational reasoning*, substituting one expression for another with
the same denotion.

----

## λπ-Ware overview

A typed first-order embedded language for describing hardware.

- deeply embedding in Agda;

- abstracts over primitive types and gates used to define circuits;

- enforces type- and scope safety by construction;

- has a *compositional* denotational semantics.

----

## Types 

We begin by defining (the syntax of) the types of our DSL:

```agda
data U : Set where
  𝟙       : U                         -- the unit type
  ι       : U                         -- the base type, such as bits
  _⊗_ _⊕_ : (σ τ : U ) → U            -- (co)products
  vec     : (τ : U) (n : ℕ) → U       -- vectors
```

Our definition is parametrised by the base type `ι` -- representing
primitive values. 

This could be bits -- but you may want to choose another (richer)
type, such as machine instructions, words, etc.

. . .

We can map these types to regular Agda types defining a function:

```
Val : (B : Set) → U → Set
```

---

## Primitive gates

What collection of primitive gates should we choose?

We could fix a collection of atomic boolean gates -- AND, OR, NOT,
XOR, etc. -- similar to other EDSLs such as Lava.

But this may not *always* be the right choice.

Instead, we will abstract over the exact choice of primitive gates
used to define more complex circuits.

---

## Primitive Gates

```agda
Ctx = List U

record Gates : Set where
  field
    gIx  : Set               -- a type representing the choice of gates
    gCtx : (g : gIx) → Ctx   -- the inputs associated with each gate
    gOut : (g : gIx) → U     -- the output of each gate
```

. . . 

```agda
-- Example gate library
data BoolGates : Set where AND OR XOR NOT : BoolGates

boolGates = record {gIx = BoolGates ; ...}
```

. . .

Now we can finally give the syntax of our DSL.

---

## Syntax of circuit terms (selection)

```agda
data λB[_] (G : Gates) (Γ : Ctx) (τ : U) : Set where
  var      : τ ∈ Γ → λB[ G ] Γ τ
  gate     : (g : gIx G) → (Δ ⊇ gCtx g)  → λB[ G ] Δ (gOut g)
  let'     : λB[ G ] Γ σ  → λB[ G ] (σ :: Γ) τ  → λB[ G ] Γ τ
  loop     : λB[ G ] (σ :: Γ) (σ ⊗ τ)  → λB[ G ] Γ τ
  ...
```

The type of circuits is parametrised by the primitive gate library,
the inputs to the circuit (or context `Γ`), and the type produced
`τ`.


---

## Syntax of circuit terms (selection)

```agda
data λB[_] (G : Gates B) (Γ : Ctx) (τ : U) : Set where
  var      : τ ∈ Γ → λB[ G ] Γ τ
  gate     : (g : gIx G) → (p : Δ ⊇ gCtx g)  → λB[ G ] Δ (gOut g)  
  let'     : λB[ G ] Γ σ  → λB[ G ] (σ :: Γ) τ  → λB[ G ] Γ τ
  loop     : λB[ G ] (σ :: Γ) (σ ⊗ τ)  → λB[ G ] Γ τ
  ...
```

* To refer to a specific input, we use the `var` constructor.

* The `gate` constructor introduces one of the primitive gates. Note
  that we have to provide inputs to the gate, from the variables
  currently in scope by proving `Δ ⊇ gCtx g`.

* We can use `let` bindings to construct intermediate results.

* The `loop` constructor introduces a hidden state of type `σ`,
  enabling the definition of sequential circuits.

----

## Semantics

We would like to define a simple evaluation function / semantics:

```agda
eval : λB[ G ] Γ τ → Env Γ → Val τ
```

Given a circuit and an environment storing values for its inputs, we
simulate the circuit for one clock cycle and produce the corresponding
output value.

. . . 

Unfortunately, it is not quite so simple:

- we need further information about the semantics of primitive gates (easy);

- this does not yet account for loops (harder).

---

## Handling loops


- For every given circuit, we can *compute* the states used by its
  loops (and primitive gates).
  
- Given this information as an additional input, we can compute the
  value produced and new state after one clock cycle.
  
- This gaves rise to a Mealy-machine semantics of the form:

```agda
eval : (c : λB[ G ] Γ τ) → State c → Env Γ →  Val τ × State c
eval (var i) s env               = lookup i env
eval (let' x body) (s1 , s2) env = let xVal = eval x s1 env in
                                   eval body s2 (x :: env) 
eval (loop body) s env           = eval (env :: s) body
...
```

Repeatedly appling the `eval` function gives the usual
stream-to-stream semantics.

----

## Simple denotational semantics

So what? 

This semantics is *compositional* - we can freely replace equals by
equals, without changing the meaning of a circuit.

We can even swap out the underlying types and primitive gate
libraries, 'automatically' preserving a circuit's semantics.

---

## Mapping between types

During the design of a circuit, you may start with a data type
modelling a small instruction set with load, store, and arithmetic
operations.

```agda
data Instr : Set where
  LOAD STORE ADD MUL : Instr
```

But at some point, you want to nail down how these are mapped to bits.

How can we do this?

. . . 

We are free to vary the base type -- and convert between abstract and
concrete representations accordingly:

```agda
⇓τ :  (B → C) →           -- using the encoding of constants
      Val B τ → Val C τ   -- map this over all values
...                       -- easy to extend to contexts, etc.
```

---

## Gate library refinement

In the same style, we can implement one (high level) library of
primitive gates in terms of another (low level) library.

To do so we need:

- to map each high level to gate of type `τ` to a low level gate of
  type `(⇓τ τ)`

- to prove that the resulting low level gate behaves the same as
  running the high level gate & converting the result.
  
(Draw a picture!)

---

## Whole circuit refinement

Each such gate library refinement gives rise to a transformation from
one circuit to another:

The circuit refinement replaces *all* gates with their corresponding implementation.

We can prove that this is guaranteed to *preserve* the semantics of
the original circuit.

. . .

This is a result of the *compositional* nature of our semantics: the
correctness of the whole translation follows from the correctness of
the translation of primitive gates.

---

## Case study (in progress) - simple microprocessor

* Simple instruction set, memory register and ALU
  + Load and store operations
  + Arithmetic over unsigned (positive) integers
  + Basic bitwise binary operations

* The ALU is built of a Logic Unit and Arithmetic Unit
  + Total gate library used is also a combination of sublibraries
  + The two parts need to use the same base type (bits)

* We get verified refinement of the ALU from the implementation proofs of each gate
  + Verification for circuit generators - any size words

* We can refine complex components (adders, multipliers, etc.) to
  their implementation in terms of boolean gates -- semantic
  preservation only needs to be proven 'locally' (at the gate level)
  -- the overall preservation then follows.

## Future work

* Better modular construction of gate libraries
  + Especially being able to _combine different base types_
  + May require fundamental changes in DSL (typing discipline)

* Congruence of refinement with other transformations:
  + we may transform a circuit, then refine or refine then
    transform. When do the two commute?

* Make it easier to write circuits by hand (rather than hand-coding De Bruijn indices);

* Synthesis

## {.standout}

Questions?



<!-- ## Overview -->

<!-- * Technology mapping -->
<!-- * DSL language overview (λπ-Ware) -->
<!--   + Syntax of types and terms -->
<!--     - Parameterisation over types and gates -->
<!--   + Semantics overview -->
<!--     - Semantics of gates and of whole circuits -->
<!-- * Gate library refinement -->
<!--   + Semantic preservation -->
<!-- * Illustrative case study -->
<!-- * (Near) future work -->


<!-- ---- -->

<!-- ## Relation to programming languages -->

<!-- A semantics is said to be *compositional* if, given two programs `p₁` -->
<!-- and `p₂` such that `〚 p₁ `〛 ~  `〚 p₂ 〛` then for all contexts C, we have -->
<!-- `〚 C [p₁] 〛` ~ `〚 C [p₂] 〛`. -->

<!-- . . .  -->

<!-- In other words, we can replace equals with equals - without changing -->
<!-- the meaning of our program. -->


