---
author: Wouter Swierstra
title: My Title
subtitle: What a great subtitle
institute: Utrecht University
theme: metropolis
mainfont: Merriweather
mainfontoptions: Scale=0.8
sansfont: Open Sans
sansfontoptions: Scale=0.8
monofont: Droid Sans Mono
monofontoptions: Scale=0.8
classoption: aspectratio=169
---

## Hello world

Here is an example

* a bulleted
* list
* a third item
* yet another item

\textrm{Merriweather Regular}

# A new section


## And one more slide

* This slide

. . .

* Has a pause in it.

---

## What about code?

We can even have code...

```haskell
data List a = Nil | Cons a (List a)
```

with (some) syntax highlighting


# How about a new section?

---

## Alerts? Bold?

We can have **bold text**

We can also have \alert{alerted text} if you must

---

## {.standout}

Questions?

<!-- Local Variables:  -->
<!-- pandoc/write: "beamer" -->
<!-- pandoc/pdf-engine: "xelatex" -->
<!-- pandoc/template: "include/beamer-template.tex" -->
<!-- pandoc/slide-level: "2" -->
<!-- End: -->
